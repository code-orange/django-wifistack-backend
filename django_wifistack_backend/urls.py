from django.conf import settings
from django.contrib import admin
from django.urls import include, path
from django.urls import re_path

urlpatterns = [
    path("", include("django_wifistack_migration.django_wifistack_migration.urls")),
    re_path(r"", include("user_sessions.urls", "user_sessions")),
    path("admin/", admin.site.urls),
    re_path(r"^watchman/", include("watchman.urls")),
    path("api/", include("django_wifistack_deviceapi.django_wifistack_deviceapi.urls")),
    re_path(
        r"^firmware/gluon/",
        include("django_wifistack_gluon.django_wifistack_gluon.urls"),
    ),
]

if settings.DEBUG:
    import debug_toolbar

    urlpatterns = [
        path("__debug__/", include(debug_toolbar.urls)),
    ] + urlpatterns
